# coding=utf-8
# 项目03_知乎数据清洗整理和结论研究
import pandas as pd
import matplotlib.pyplot as plt
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题
# 1.读取
zhihu = pd.read_csv('C:/Users/louis/Desktop/Python数据分析学习/项目03知乎数据清洗整理和结论研究_资料/知乎数据_201701.csv', engine='python')
people = pd.read_csv('C:/Users/louis/Desktop/Python数据分析学习/项目03知乎数据清洗整理和结论研究_资料/六普常住人口数.csv', engine='python')


# print(zhihu.head())
# print(people.head())

# 2.清洗
def f1(df):
    cols = df.columns
    for col in cols:
        if df[col].dtype == 'object':
            df[col].fillna('缺失数据', inplace=True)
        else:
            df[col].fillna(0, inplace=True)
    return (df)


after = f1(zhihu)
print(after.head(10))

df_city = after.groupby('居住地').count()

people['city'] = people['地区'].str[:-1]
# print(df_city.head())
# print(data2.head())

result = pd.merge(df_city, people,
                  left_index=True,
                  how='inner',
                  right_on='city')[['_id', 'city', '常住人口']]
result['知友密度'] = result['_id'] / result['常住人口']
print(result.head())


def data_nor(df, *cols):
    colnames = []
    for col in cols:
        colname = col + '_nor'
        df[colname] = (df[col] - df[col].min()) / (df[col].max() - df[col].min()) * 100
        colnames.append(colname)
    return (df, colnames)


#
resultdata = data_nor(result, '_id', '知友密度')[0]
resultcolnames = data_nor(result, '_id', '知友密度')[1]
data1 = resultdata.sort_values(resultcolnames[0], ascending=False)[['city', resultcolnames[0]]].iloc[:20]
data2 = resultdata.sort_values(resultcolnames[1], ascending=False)[['city', resultcolnames[1]]].iloc[:20]
print(data2)
# 标准化取值后得到知友数量，知友密度的TOP20数据
#
fig1 = plt.figure(num=1, figsize=(12, 4))
y1 = data1[resultcolnames[0]]
plt.bar(range(20),
        y1,
        width=0.8,
        tick_label=data1['city'])
plt.title('知友数量前20城市\n')
plt.show()
for i, j in zip(range(20), y1):
    plt.text(i + 0.1, 2, '%.1f' % j, color='k', fontsize=9)

fig2 = plt.figure(num=2, figsize=(12, 4))
y2 = data2[resultcolnames[1]]
plt.bar(range(20),
        y2,
        width=0.8,
        edgecolor='k',
        tick_label=data2['city'])
plt.title('知友密度前20城市\n')
plt.show()
for i, j in zip(range(20), y2):
    plt.text(i + 0.1, 2, '%.1f' % j, color='k', fontsize=9)

q2data = after.groupby('教育经历').sum()[['关注', '关注者']].drop(['缺失数据', '大学', '本科'])
result2 = q2data.sort_values('关注', ascending=False)[:20]

plt.figure(figsize=(10, 6))
x = result2['关注']
y = result2['关注者']
follow_mean = result2['关注'].mean()
fans_mean = result2['关注者'].mean()
plt.scatter(x, y, marker='.',
            s=y / 1000,
            cmap='Blues',
            c=y,
            alpha=0.8,
            label='学校')
plt.show()

plt.axvline(follow_mean, hold=None, label="平均关注人数：%i人" % follow_mean, color='r')
plt.axhline(fans_mean, hold=None, label="平均粉丝数：%i人" % fans_mean, color='k')
plt.legend(loc='upper left')
# 添加显示内容

for i, j, n in zip(x, y, result2.index):
    plt.text(i + 500, j, n, color='k')
# 添加注释
plt.show()
