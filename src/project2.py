# coding=utf-8
# 项目02_基于Python的算法函数创建
# 题目1：有1、2、3、4个数字，能组成多少个互不相同且无重复数字的两位数？都是多少？
# # 该题目不用创建函数
#
# 提交方式：
# 需要学员将答案代码以Python语言的形式，复制粘贴进答题框
source = [1,2,3,4]
count = 0
for num1 in source:
    for num2 in source:
        if num1 != num2:
            num = '%i%i' %(num2,num1)
            count = count+1
            print(num)
print('一共有%i个两位数' %count)
# 题目2：输入三个整数x,y,z，请把这三个数由小到大输出，可调用input()。（需要加判断：判断输入数据是否为数字）
# 提示：判断是否为数字：.isdigit()
# 该题目需要创建函数

def f(n):
    list = []
    for i in range(0,n):
        num = input("输入一个数字吧")
        while(num.isdigit() == False):
            num = input("不行，重新输一个")
        list.append(num)
    return list

source =  sorted(f(3))
print(source)
# 题目3：输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。
# 提示：利用while语句,条件为输入的字符不为'\n'.
# 该题目不需要创建函数

str = input("请乱打一行字母数字和空格")
zimu = 0
kongge = 0
shuzi = 0
qita = 0

for s in str:
    if s.isdigit():
        shuzi+=1
    elif s.isspace():
        kongge+=1
    elif s.isalpha():
        zimu+=1
    else:
        qita+=1
print('一共有%i个字母%i个空格%i个数字，还有%i个啥都不是' %(zimu,kongge,shuzi,qita))
# 题目4：猴子吃桃问题：猴子第一天摘下若干个桃子，当即吃了一半，还不瘾，又多吃了一个,第二天早上又将剩下的桃子吃掉一半，又多吃了一个。
#  以后每天早上都吃了前一天剩下的一半零一个。到第10天早上想再吃时，见只剩下一个桃子了。求第一天共摘了多少?
# 提示：采取逆向思维的方法，从后往前推断。
# 该题目不需要创建函数

num = 1
for i in range(1,10):
    num = num+1
    num = num*2
    print(num)
print('第一天一共摘了%i个桃子' %num)
# 题目5：猜数字问题，要求如下：
# ① 随机生成一个整数
# ② 猜一个数字并输入
# ③ 判断是大是小，直到猜正确
# ④ 判断时间
# 提示：需要用time模块、random模块
# 该题目不需要创建函数
import random
import time
answer = random.randint(0,100)
print('开始猜数字啦')
n = input('来猜一个呗')
mytime = time.time()
while n.isdigit()==False:
    n=input('老老实实输数字好不好，重新输一个')
n = int(n)
while n!=answer:
    if n>answer:
        print('大了大了')
    else:
        print('小了小了')
    n = input('再猜')
    while n.isdigit() == False:
        n = input('老老实实输数字好不好，重新输一个')
    n = int(n)
print('对啦对啦，答案就是%i'%answer)
time = time.time()-mytime
print('一共用时%i秒'%time)
