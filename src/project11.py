import pandas as pd
from pylab import mpl
import numpy as np

mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题
import warnings

warnings.filterwarnings("ignore")

# 数据读取
import os

os.chdir("C:/Users/louis/DataAnalysis/source/项目11/")
df1 = pd.read_excel("moviedata.xlsx", sheetname=0)
df = df1[df1['豆瓣评分'] > 0]

print('成功加载数据%i条' % df.__len__())

# 豆瓣评分情况
df['豆瓣评分'].describe()
df['豆瓣评分'].plot.hist(bins=20, color='red')
df['豆瓣评分'].plot.box(vert=False, grid=True, figsize=(10, 4))

# 正态分布判断
from scipy import stats

u = df['豆瓣评分'].mean()
std = df['豆瓣评分'].std()
print(stats.kstest(df['豆瓣评分'], 'norm', (u, std)))

# 4.3分作为一个烂片标准
data_lp = df[df['豆瓣评分'] < 4.3].reset_index()
lp_top20 = data_lp[['电影名称', '豆瓣评分', '主演', '导演']].sort_values(by='豆瓣评分').iloc[:20]

# 筛选题材的类型
typelst = []
for i in df[df['类型'].notnull()]['类型'].str.replace(' ', "").str.split('/'):
    typelst.extend(i)

typelst = list(set(typelst))
print('一共有%i个电影题材' % typelst.__len__())

df_type = df[df['类型'].notnull()]


def f1(data, typei):
    dic_type_lp = {}
    datai = data[data['类型'].str.contains(typei)]
    # 计算烂片比例
    lp_pre_i = len(datai[datai['豆瓣评分'] < 4.3]) / len(datai)
    dic_type_lp['typeneme'] = typei
    dic_type_lp['typecount'] = len(datai)
    dic_type_lp['type_lp_pre'] = lp_pre_i
    return dic_type_lp


list_type_lp = []
for i in typelst:
    dici = f1(df_type, i)
    list_type_lp.append(dici)

df_type_lp = pd.DataFrame(list_type_lp)
type_lp_top20 = df_type_lp.sort_values(by="type_lp_pre", ascending=False).iloc[:20]

from bokeh.plotting import figure, show, output_file
from bokeh.models import ColumnDataSource
from bokeh.models import HoverTool

# 画图
type_lp_top20['size'] = type_lp_top20['typecount'] ** 0.5 * 2
source = ColumnDataSource(type_lp_top20)
lst_type = type_lp_top20['typeneme'].tolist()

hover = HoverTool(tooltips=[
    ("数据量", "@typecount"),
    ("烂片率", "@type_lp_pre")
])
output_file('project11_h1.html')
p = figure(x_range=lst_type, plot_width=800, plot_height=500, title="不同电影题材的烂片比例",
           tools=[hover, "box_select,reset,xwheel_zoom,pan,crosshair"])

p.circle(x='typeneme', y='type_lp_pre', source=source, size='size',
         line_color='black', line_dash=[6, 4], fill_color='red', fill_alpha=0.5)
# show (p)
df_loc = df[['电影名称', '制片国家/地区', '豆瓣评分']][df['制片国家/地区'].notnull()]
df_loc = df_loc[df_loc['制片国家/地区'].str.contains('中国大陆')]
print('有效数据%i条' % df_loc.__len__())

loclst = []
for i in df_loc['制片国家/地区'].str.replace(" ", "").str.split("/"):
    loclst.extend(i)
loclst = list(set(loclst))
loclst.remove("中国大陆")
loclst.remove("香港")
loclst.remove("台湾")
loclst.remove("中国")


def f2(data, loci):
    dic_loc_lp = {}
    datai = data[data['制片国家/地区'].str.contains(loci)]
    # 计算烂片比例
    lp_pre_i = len(datai[datai['豆瓣评分'] < 4.3]) / len(datai)
    dic_loc_lp['locneme'] = loci
    dic_loc_lp['loccount'] = len(datai)
    dic_loc_lp['loc_lp_pre'] = lp_pre_i
    return dic_loc_lp


lst_loc_lp = []
for i in loclst:
    dici = f2(df_loc, i)
    lst_loc_lp.append(dici)

df_loc_lp = pd.DataFrame(lst_loc_lp)
df_loc_lp = df_loc_lp[df_loc_lp['loccount'] >= 3]
loc_lp_top20 = df_loc_lp.sort_values(by="loc_lp_pre", ascending=False).iloc[:20]

# 演员阵容与烂片的关系
# 按照主演统计烂片比例
df['主演人数'] = df['主演'].str.split("/").str.len()
df_leadrole1 = df[['主演人数', '豆瓣评分']].groupby('主演人数').count()
df_leadrole2 = df[['主演人数', '豆瓣评分']][df['豆瓣评分'] < 4.3].groupby('主演人数').count()
df_leadrole_pre = pd.merge(df_leadrole1, df_leadrole2, left_index=True, right_index=True)
df_leadrole_pre.columns = ['电影数量', '烂片数量']

df_leadrole_pre.reset_index(inplace=True)
df_leadrole_pre['主演人数分类'] = pd.cut(df_leadrole_pre['主演人数'], [0, 2, 4, 6, 9, 50],
                                   labels=['1-2人', '3-4人', '5-6人', '7-9人', '10人以上', ])

df_leadrole_pre2 = df_leadrole_pre[['主演人数分类', '电影数量', "烂片数量"]].groupby('主演人数分类').sum()
df_leadrole_pre2["烂片比例"] = df_leadrole_pre2['烂片数量'] / df_leadrole_pre2['电影数量']

# 不同主演的烂片比例
df_role1 = df[(df['豆瓣评分'] < 4.3) & (df['主演'].notnull())]
df_role2 = df[df['主演'].notnull()]
leadrolelst = []
for i in df_role1['主演'][df_role1['主演'].notnull()].str.replace(' ', '').str.split('/'):
    leadrolelst.extend(i)
# 取出所有电影的主演，并整理成列表
# 注意这里要删除“主演”中的空格字符

leadrolelst = list(set(leadrolelst))
print('筛选后的主演演员人数为%i人' % len(leadrolelst))
lst_role_lp = []
for i in leadrolelst:
    datai = df_role2[df_role2['主演'].str.contains(i)]
    if len(datai) > 2:
        dic_role_lp = {}
        lp_pre_i = len(datai[datai['豆瓣评分'] < 4.3]) / len(datai)
        dic_role_lp['role'] = i
        dic_role_lp['rolecount'] = len(datai)
        dic_role_lp['role_lp_pre'] = lp_pre_i
        lst_role_lp.append(dic_role_lp)

df_role_lp = pd.DataFrame(lst_role_lp)
role_lp_top20 = df_role_lp.sort_values(by='role_lp_pre', ascending=False).iloc[:20]

# 导演烂片年产量
df_year = df[['电影名称', '导演', '豆瓣评分', '上映日期']][df['导演'].notnull()]
df_year = df_year[df_year['上映日期'].notnull()]
df_year['上映日期'] = df_year['上映日期'].str.replace(' ', '')
df_year['year'] = df_year['上映日期'].str[:4]
df_year = df_year[df_year['year'].str[0] == '2']
df_year['year'] = df_year['year'].astype(np.int)
print('年份有效数据%i条' % df_year.__len__())
df_year.head()

# 筛选导演
directorlst = []
for i in df_year['导演'].str.replace(' ', '').str.split('/'):
    directorlst.extend(i)

directorlst = list(set(directorlst))
print('导演有效数据%i' % len(directorlst))

lst_dir_lp = []
for i in directorlst:
    datai = df_year[df_year['导演'].str.contains(i)]
    if len(datai) > 10:
        dic_dir_lp = {}
        lp_pre_i = len(datai[datai['豆瓣评分'] < 4.3]) / len(datai)
        # 计算该主演烂片比例
        dic_dir_lp['dir'] = i
        dic_dir_lp['dircount'] = len(datai)
        dic_dir_lp['dir_lp_pre'] = lp_pre_i
        lst_dir_lp.append(dic_dir_lp)

df_dir_lp = pd.DataFrame(lst_dir_lp)


def f3(data, diri):
    datai = data[data['导演'].str.contains(diri)]
    # 筛选数据
    data_moviecount = datai[['year', '电影名称']].groupby('year').count()
    data_scoremean = datai[['year', '豆瓣评分']].groupby('year').mean()
    df_i = pd.merge(data_moviecount, data_scoremean, left_index=True, right_index=True)
    df_i.columns = ['count', 'score']
    df_i['size'] = df_i['count'] * 5
    return (df_i)


dirdata1 = f3(df_year, '王晶')
dirdata2 = f3(df_year, '周伟')
dirdata3 = f3(df_year, '徐克')
dirdata4 = f3(df_year, '邓衍成')

from bokeh.models.annotations import BoxAnnotation

hover = HoverTool(tooltips=[("该年电影均分", "@score"),
                            ("该年电影产量", "@count")])  # 设置标签显示内容
p = figure(plot_width=900, plot_height=500, title="不同导演每年的电影产量及电影均分",
           tools=[hover, 'reset,xwheel_zoom,pan,crosshair,box_select'])
# 构建绘图空间
output_file('project11_h2.html')
source1 = ColumnDataSource(dirdata1)
p.circle(x='year', y='score', source=source1, size='size', legend="王晶", fill_color='olive', fill_alpha=0.7,
         line_color=None)
# 绘制散点图1
source2 = ColumnDataSource(dirdata2)
p.circle(x='year', y='score', source=source2, size='size', legend="周伟", fill_color='blue', fill_alpha=0.7,
         line_color=None)
# 绘制散点图2
source3 = ColumnDataSource(dirdata3)
p.circle(x='year', y='score', source=source3, size='size', legend="徐克", fill_color='green', fill_alpha=0.7,
         line_color=None)
# 绘制散点图3
source4 = ColumnDataSource(dirdata4)
p.circle(x='year', y='score', source=source4, size='size', legend="邓衍成", fill_color='gray', fill_alpha=0.7,
         line_color=None)
# 绘制散点图4

bg = BoxAnnotation(top=4.4, fill_alpha=0.1, fill_color='red')
p.add_layout(bg)
# 绘制烂片分隔区域

p.xgrid.grid_line_dash = [10, 4]
p.ygrid.grid_line_dash = [10, 4]
p.legend.location = "top_right"
# 设置其他参数

show(p)
