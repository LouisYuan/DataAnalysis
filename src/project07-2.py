# coding=utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题
# 忽略警告
import warnings
warnings.filterwarnings("ignore")
"""
2、选择一个餐饮类型，在qgis中做将上海划分成格网空间，结合python辅助做空间指标评价，得到餐饮选址位置
* 课程这里以“素菜馆为例”
课程数据
① net_population.shp → 投影坐标系，上海1km²格网内的人口密度数据
② road.shp → 投影坐标西，上海道路数据
要求：
① 通过空间分析，分别计算每个格网内的几个指标：人口密度指标、道路密度指标、餐饮热度指标、同类竞品指标
② 评价方法：
     人口密度指标 → 得分越高越好
     道路密度指标 → 得分越高越好
     餐饮热度指标 → 得分越高越好
     同类竞品指标 → 得分越低越好
     综合指标 = 人口密度指标*0.4 + 餐饮热度指标*0.3 + 道路密度指标*0.2 +同类竞品指标*0.1
③ 最后得到较好选址的网格位置的中心坐标，以及所属区域
   * 可以用bokeh制作散点图
提示：
① 道路密度指标计算方法 → 网格内道路长度
② 餐饮热度指标计算方法 → 网格内餐饮poi计数
③ 同类竞品指标计算方法 → 网格内素菜馆poi计数
④ 餐饮poi数据记得投影
⑤ 可以以“net_population.shp”为网格基础数据，做空间统计
⑥ 在qgis做空间统计之后，网格数据导出点数据，投影成wgs84地理坐标系，导出excel数据，在python做指标标准化等
⑦ 在bokeh中做散点图时，注意添加一个size字段，通过最终评分来赋值
⑧ 在bokeh中做散点图时，可以给TOP10的点用颜色区分
"""

# 作图文件
from bokeh.plotting import figure, show, output_file
# 数据整理
from bokeh.models import ColumnDataSource
# 加载数据
import os
os.chdir("C:/Users/louis/DataAnalysis/source/项目7")
df1 = pd.read_excel("result.xlsx", sheetname=0)

# 以下是作图
from bokeh.layouts import gridplot
from bokeh.models import HoverTool
from bokeh.models.annotations import BoxAnnotation


df1.fillna(0,inplace = True)
df1.columns = ['lng','lat','人口密度','道路长度','餐饮计数','竞争计数']

df1['rkmd_norm'] = (df1["人口密度"] - df1['人口密度'].min())/(df1['人口密度'].max() - df1['人口密度'].min())
df1['cyrd_norm'] = (df1["餐饮计数"] - df1['餐饮计数'].min())/(df1['餐饮计数'].max() - df1['餐饮计数'].min())
df1['tljp_norm'] = (df1["竞争计数"] - df1['竞争计数'].min())/(df1['竞争计数'].max() - df1['竞争计数'].min())
df1['dlcd_norm'] = (df1["道路长度"] - df1['道路长度'].min())/(df1['道路长度'].max() - df1['道路长度'].min())

df1["final_score"] = df1['rkmd_norm']*0.4 + df1['cyrd_norm']*0.3 + df1['dlcd_norm']*0.2 + df1['tljp_norm'] * 0.1
final = df1.sort_values(by ="final_score", ascending= False).reset_index()

print(final.head())

final['size'] = final['final_score'] * 20
final['color'] = 'green'
final['color'].iloc[:10] = 'red'


output_file('project7_h2.html')
source = ColumnDataSource(final)
hover = HoverTool(tooltips=[
    ("经度", "@lng"),
    ("纬度", "@lat"),
    ("最终得分", "@final_score")
])

result = figure(plot_width=800, plot_height=800, title="空间散点图",
                tools=[hover, "box_select,reset,wheel_zoom,pan,crosshair"])

result.square(x = 'lng',y = 'lat', source = source,
              line_color= 'black' , fill_alpha = 0.5,
              size = 'size' , color = 'color'
              )
show(result)