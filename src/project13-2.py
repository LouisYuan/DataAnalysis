# coding=utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import time
import warnings

warnings.filterwarnings("ignore")

person_p = [0.899 / 90 for i in range(100)]
for i in [1, 11, 21, 31, 41, 51, 61, 71, 81, 91]:
    person_p[i - 1] = 0.0101


def game3(data, roundi):
    round_i = pd.DataFrame({'pre_round': data[roundi - 1], 'lost': 1})
    choice_i = pd.Series(np.random.choice(person_n, 100, p=person_p))

    gain_i = pd.DataFrame({'gain': choice_i.value_counts()})
    round_i = round_i.join(gain_i)
    round_i.fillna(0, inplace=True)
    return round_i['pre_round'] - round_i['lost'] + round_i['gain']


person_n = [x for x in range(1, 101)]

fortune = pd.DataFrame([100 for i in range(100)], index=person_n)
fortune.index.name = 'id'

for round in range(1, 17001):
    fortune[round] = game3(fortune, round)
    if round % 1000 == 0:
        print('已经完成%i轮' % round)
game3_result = fortune.T


def graph3(data, start, end, length):
    for n in list(range(start, end, length)):
        datai = pd.DataFrame({'money': data.iloc[n], 'color': 'gray'})
        datai['color'].loc[[1, 11, 21, 31, 41, 51, 61, 71, 81, 91]] = 'red'
        datai = datai.sort_values(by='money').reset_index()
        plt.figure(figsize=(10, 6))
        plt.bar(datai.index, datai['money'], color=datai['color'], alpha=0.8, width=0.9)
        plt.ylim([-300, 500])
        plt.xlim([-10, 110])
        plt.title('Round %d' % n)
        plt.xlabel('playerID')
        plt.ylabel("fortune")
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.savefig('game3_round%d.png' % n)


os.chdir('C:/Users/louis/DataAnalysis/source/项目13/game3')
data0 = pd.DataFrame({'money':game3_result.iloc[0],'color':'gray'})
data0['color'].loc[[1, 11, 21, 31, 41, 51, 61, 71, 81, 91]] = 'red'

graph3(game3_result, 0, 100, 10)
graph3(game3_result, 100, 1000, 100)
graph3(game3_result, 1000, 17001, 400)