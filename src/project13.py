# coding=utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import time
import warnings

warnings.filterwarnings("ignore")


# 项目13

def game1(data, roundi):
    if len(data[data[roundi - 1] == 0]) > 0:
        round_i = pd.DataFrame({'pre_round': data[roundi - 1], 'lost': 0})
        con = round_i['pre_round'] > 0
        round_i['lost'][con] = 1
        round_players_i = round_i[con]
        choice_i = pd.Series(np.random.choice(person_n, len(round_players_i)))
        gain_i = pd.DataFrame({'gain': choice_i.value_counts()})
        round_i = round_i.join(gain_i)
        round_i.fillna(0, inplace=True)
        return round_i['pre_round'] - round_i['lost'] + round_i['gain']
    else:
        round_i = pd.DataFrame({'pre_round': data[roundi - 1], 'lost': 1})
        choice_i = pd.Series(np.random.choice(person_n, 100))
        gain_i = pd.DataFrame({'gain': choice_i.value_counts()})
        round_i = round_i.join(gain_i)
        round_i.fillna(0, inplace=True)
        return round_i['pre_round'] - round_i['lost'] + round_i['gain']


def game2(data, roundi):
    round_i = pd.DataFrame({'pre_round': data[roundi - 1], 'lost': 1})
    choice_i = pd.Series(np.random.choice(person_n, 100))
    gain_i = pd.DataFrame({'gain': choice_i.value_counts()})
    round_i = round_i.join(gain_i)
    round_i.fillna(0, inplace=True)
    return round_i['pre_round'] - round_i['lost'] + round_i['gain']


person_n = [x for x in range(1, 101)]
fortune = pd.DataFrame([100 for i in range(100)], index=person_n)
fortune.index.name = 'id'

for round in range(1, 17001):
    fortune[round] = game2(fortune, round)
    if round % 1000 == 0:
        print('已经完成%i轮' % round)
game1_result = fortune.T

os.chdir('C:/Users/louis/DataAnalysis/source/项目13/game2_red')


def graph1(data, start, end, length):
    for n in list(range(start, end, length)):
        datai = data.iloc[n]
        plt.figure(figsize=(10, 6))
        plt.bar(datai.index, datai.values, color='gray', alpha=0.8, width=0.9)
        plt.ylim([-300, 500])
        plt.xlim([-10, 110])
        plt.title('Round %d' % n)
        plt.xlabel('playerID')
        plt.ylabel("fortune")
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.savefig('game2_round%d.png' % n)


def graph2(data, start, end, length):
    for n in list(range(start, end, length)):
        datai = data.iloc[n].sort_values().reset_index()[n]
        plt.figure(figsize=(10, 6))
        plt.bar(datai.index, datai.values, color='gray', alpha=0.8, width=0.9)
        plt.ylim([-300, 500])
        plt.xlim([-10, 110])
        plt.title('Round %d' % n)
        plt.xlabel('playerID')
        plt.ylabel("fortune")
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.savefig('game2_round%d.png' % n)


def graph3(data, start, end, length):
    for n in list(range(start, end, length)):
        datai = pd.DataFrame({'money': data.iloc[n], 'color': 'gray'})
        datai['color'].loc[id_pc] = 'red'
        datai = datai.sort_values(by='money').reset_index()
        plt.figure(figsize=(10, 6))
        plt.bar(datai.index, datai['money'], color=datai['color'], alpha=0.8, width=0.9)
        plt.ylim([-300, 500])
        plt.xlim([-10, 110])
        plt.title('Round %d' % n)
        plt.xlabel('playerID')
        plt.ylabel("fortune")
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.savefig('game2_round%d.png' % n)


game1_result.iloc[17000].max()
# 最大367.0        454.0
# 10%的人占28.14   34.68
# 20% 占46.58      57.64
# 59个人缩水       50

graph1(game1_result, 0, 100, 10)
graph1(game1_result, 100, 1000, 100)
graph1(game1_result, 1000, 17001, 400)
graph2(game1_result, 0, 100, 10)
graph2(game1_result, 100, 1000, 100)
graph2(game1_result, 1000, 17001, 400)
graph3(game1_result, 0, 100, 10)
graph3(game1_result, 100, 1000, 100)
graph3(game1_result, 1000, 17001, 400)
#
round_17000_1 = pd.DataFrame({'money': game1_result.iloc[17000]}).sort_values(by='money', ascending=False).reset_index()
round_17000_1['pre'] = round_17000_1['money'] / round_17000_1['money'].sum()
round_17000_1['cumsum'] = round_17000_1['pre'].cumsum()

# 游戏次数与标准差的关系
game2_std = game1_result.std(axis=1)
game2_std.plot(figsize=(12, 5), color='red', alpha=0.6, grid=True)
plt.show()

game2_round_6200 = pd.DataFrame({'money': game1_result.iloc[6200].sort_values().reset_index()[6200],
                                 'id': game1_result.iloc[6200].sort_values().reset_index()["id"],
                                 'color': 'gray'})
game2_round_6200['color'][game2_round_6200['money'] < 0] = 'red'
id_pc = game2_round_6200['id'][game2_round_6200['money'] < 0].tolist()
