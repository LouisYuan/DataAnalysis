# coding=utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题
# 忽略警告
import warnings

warnings.filterwarnings("ignore")
# 作图文件
from bokeh.plotting import figure, show, output_file
# 数据整理
from bokeh.models import ColumnDataSource

# 加载数据
import os

os.chdir("C:/Users/louis/DataAnalysis/source/项目8/")
df = pd.read_excel("双十一淘宝美妆数据.xlsx", sheetname=0)
df.fillna(0, inplace=True)
df.index = df['update_time']
df['date'] = df.index.day
# 双十一在售的占比情况
data1 = df[['id', 'title', '店名', 'date']]
d1 = data1[['id', 'date']].groupby(by='id').agg(['min', 'max'])['date']
id_11 = data1[data1['date'] == 11]['id']
d2 = pd.DataFrame({'id': id_11, '双十一当天是否售卖': True})
id_data = pd.merge(d1, d2, left_index=True, right_on='id', how='left')
id_data.fillna(False, inplace=True)

m_pre = len(id_11) / len(d1)

print('双十一当天参与活动的商品%i个，占比%.2f%%' % (len(id_11), m_pre * 100))

# 商品销售方式分类

id_data['type'] = '待分类'
id_data['type'][(id_data['min'] < 11) & (id_data['max'] > 11)] = 'A'
id_data['type'][(id_data['min'] < 11) & (id_data['max'] == 11)] = 'B'
id_data['type'][(id_data['min'] == 11) & (id_data['max'] > 11)] = 'C'
id_data['type'][(id_data['min'] == 11) & (id_data['max'] == 11)] = 'D'
id_data['type'][id_data['双十一当天是否售卖'] == False] = 'F'
id_data['type'][id_data['max'] < 11] = 'E'
id_data['type'][id_data['min'] > 11] = 'G'

result_1 = id_data["type"].value_counts()
result_1 = result_1.loc[['A', 'B', 'C', 'D', 'E', 'F', 'G']]
print(result_1)

from bokeh.palettes import brewer

colori = brewer["YlGn"][7]
plt.axis('equal')

plt.pie(result_1, labels=result_1.index, autopct='%.2f%%', colors=colori, startangle=90, radius=1.5,
        counterclock=False)

# 未参与双十一的商品去向如何

id_not11 = id_data[id_data['双十一当天是否售卖'] == False]
data_not11 = id_not11[['id', 'type']]
df_not11 = id_not11[['id', 'type']]
data_not11 = pd.merge(df_not11, df, on='id', how='left')

id_con1 = id_data['id'][id_data['type'] == "F"].values
data_con2 = data_not11[['id', 'title', 'date']].groupby(by=['id', 'title']).count()
title_count = data_con2.reset_index()['id'].value_counts()
id_con2 = title_count[title_count > 1].index
data_con3 = data_not11[data_not11['title'].str.contains('预售')]
id_con3 = data_con3['id'].value_counts().index
print('%i个暂时下架，%i个重新上架，%i个为预售商品' % (len(id_con1), len(id_con2), len(id_con3)))

# 真正参与双十一当天活动的商品及品牌情况
id_11sale = id_11
id_11sale_final = np.hstack((id_11sale, id_con3))
result_2_i = pd.DataFrame({'id': id_11sale_final})

x1 = pd.DataFrame({'id': id_11})
x1_df = pd.merge(x1, df, on='id', how='left')
brand_11sale = x1_df.groupby('店名')['id'].count()

x2 = pd.DataFrame({'id': id_con3})
x2_df = pd.merge(x2, df, on='id', how='left')
brand_ys = x1_df.groupby('店名')['id'].count()

result_2_data = pd.DataFrame({'当天参与活动商品数量': brand_11sale, '预售商品数量': brand_ys})
result_2_data["总量"] = result_2_data['当天参与活动商品数量'] + result_2_data['预售商品数量']
result_2_data.sort_values(by='总量', inplace=True, ascending=False)

# 以下是作图
from bokeh.layouts import gridplot
from bokeh.models import HoverTool
from bokeh.models.annotations import BoxAnnotation
from bokeh.core.properties import value

lst_brand = result_2_data.index.tolist()
lst_type = result_2_data.columns.tolist()[:2]
colors = ['red', 'green']

result_2_data.index.name = 'brand'
result_2_data.columns = ['sale_on_11', 'presell', 'sum']
source = ColumnDataSource(result_2_data)
hover = HoverTool(tooltips=[
    ("品牌", "@brand"),
    ("双十一当天参与活动的商品数量", "@sale_on_11"),
    ("预售商品数量", "@presell"),
    ("真正参与双十一活动的商品总数", "@sum")
])

output_file("project8_pic1.html")
p = figure(x_range=lst_brand, plot_width=900, plot_height=350, title="双十一情况",
           tools=[hover, "reset,xwheel_zoom,pan,crosshair"])
p.vbar(top='sum', x='brand', source=source, width=0.9,
       # color=colors,
       alpha=0.7,
       # legend=[value(x) for x in lst_type],
       muted_color="black", muted_alpha=0.2)
show(p)

# coding=utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pylab import mpl
from bokeh.models import HoverTool

mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题
# 忽略警告
import warnings

warnings.filterwarnings("ignore")
# 作图文件
from bokeh.plotting import figure, show, output_file
# 数据整理
from bokeh.models import ColumnDataSource

# 加载数据
import os

os.chdir("C:/Users/louis/DataAnalysis/source/项目8/")
df = pd.read_excel("双十一淘宝美妆数据.xlsx", sheetname=0)
df.fillna(0, inplace=True)
df.index = df['update_time']
df['date'] = df.index.day

data2 = df[['id', 'title', '店名', 'date', 'price']]
data2['period'] = pd.cut(data2['date'], [4, 10, 11, 14], labels=['双十一前', '双十一当天', '双十一后'])

price = data2[['id', 'price', 'period']].groupby(['id', 'price']).min()
price.reset_index(inplace=True)

id_count = price['id'].value_counts()
id_type1 = id_count[id_count == 1].index
id_type2 = id_count[id_count != 1].index

result3_data1 = data2[['id', 'price', 'period', '店名']].groupby(['id', 'price']).min()
result3_data1.reset_index(inplace=True)
result3_before11 = result3_data1[result3_data1['period'] == '双十一前']
result3_at11 = result3_data1[result3_data1['period'] == '双十一当天']
result3_data2 = pd.merge(result3_before11, result3_at11, on='id')
result3_data2['zkl'] = result3_data2['price_y'] / result3_data2['price_x']

bokeh_data = result3_data2[['id', 'zkl']].dropna()
bokeh_data['zkl_range'] = pd.cut(bokeh_data['zkl'], bins=np.linspace(0, 1, 21))
bokeh_data2 = bokeh_data.groupby('zkl_range').count().iloc[:-1]
bokeh_data2['zkl_pre'] = bokeh_data2['zkl'] / bokeh_data2['zkl'].sum()

print(bokeh_data2.head())

source = ColumnDataSource(bokeh_data2)
lst_zkl = bokeh_data2.index.tolist()
# lst_zkl = []
# for i in lst_zkl2:
#     lst_zkl.append(str(i))

hover = HoverTool(tooltips=[("折扣率", "@zkl")])
#
# output_file("project8_pic2.html")
# p = figure(x_range=lst_zkl, plot_width=900, plot_height=350, title="商品折扣率统计",
#            tools=[hover, "reset,xwheel_zoom,pan,crosshair"])
# p.line(x='zkl_range', y='zkl_pre', source=source, line_color='black', line_dash=[10, 4])
#
# p.circle(x='zkl_range', y='zkl_pre', source=source, size=8, color='red', alpha=0.8)
# show(p)

# 问题4
# from bokeh.transform import jitter
# brands = result3_data2['店名_x'].dropna().unique().tolist()
# bokeh_data3 = result3_data2[['id', 'zkl', '店名_x']].dropna()
# # bokeh_data3[bokeh_data3['zkl']<0.96]
# source2 = ColumnDataSource(bokeh_data3)
#
# output_file('project8_pic3.html')
# p = figure(y_range=brands, plot_width=900, plot_height=700, title="不同品牌的折扣情况",
#            tools=[hover, "reset,xwheel_zoom,pan,crosshair"])
#
# p.circle(x='zkl', y= jitter('店名_x',width=0.7,range = p.y_range), source=source,alpha=0.8)
# show(p)


# 打折套路
data_zk = result3_data2[result3_data2['zkl'] < 0.95]
result4_zkld = data_zk.groupby('店名_y')['zkl'].mean()

n_dz = data_zk['店名_y'].value_counts()
n_zs = result3_data2['店名'].value_counts()

result4_dzspbl = pd.DataFrame({'打折商品数': n_dz, '商品总数': n_zs})
result4_dzspbl['打折商品比例'] = result4_dzspbl['打折商品数'] / result4_dzspbl['商品总数']

result4_dzspbl.dropna(inplace=True)
result4_sum = result_2_data.copy()

result4_data = pd.merge(pd.DataFrame(result4_zkld), result4_dzspbl, left_index=True, right_index=True,how='inner')
result4_data = pd.merge(result4_data,result4_sum,left_index= True,right_index=True,how='inner')

from bokeh.models.annotations import Span            # 导入Span模块
from bokeh.models.annotations import Label           # 导入Label模块
from bokeh.models.annotations import BoxAnnotation   # 导入BoxAnnotation模块


bokeh_data = result4_data[['zkl','sum','参与打折商品比例']]
bokeh_data.columns = ['zkl','amount','pre']
bokeh_data['size'] = bokeh_data['amount'] * 0.03
source = ColumnDataSource(bokeh_data)
# 创建ColumnDataSource数据

x_mean = bokeh_data['pre'].mean()
y_mean = bokeh_data['zkl'].mean()

hover = HoverTool(tooltips=[("品牌", "@index"),
                            ("折扣率", "@zkl"),
                            ("商品总数", "@amount"),
                            ("参与打折商品比例", "@pre"),
                           ])  # 设置标签显示内容
p = figure(plot_width=600, plot_height=600,
                title="各个品牌打折套路解析" ,
                tools=[hover,'box_select,reset,wheel_zoom,pan,crosshair'])
# 构建绘图空间

p.circle_x(x = 'pre',y = 'zkl',source = source,size = 'size',
           fill_color = 'red',line_color = 'black',fill_alpha = 0.6,line_dash = [8,3])
p.ygrid.grid_line_dash = [6, 4]
p.xgrid.grid_line_dash = [6, 4]
# 散点图

x = Span(location=x_mean, dimension='height', line_color='green',line_alpha = 0.7, line_width=1.5, line_dash = [6,4])
y = Span(location=y_mean, dimension='width', line_color='green',line_alpha = 0.7, line_width=1.5, line_dash = [6,4])
p.add_layout(x)
p.add_layout(y)
# 绘制辅助线

bg1 = BoxAnnotation(bottom=y_mean, right=x_mean,fill_alpha=0.1, fill_color='olive')
label1 = Label(x=0.1, y=0.55,text="少量大打折",text_font_size="10pt" )
p.add_layout(bg1)
p.add_layout(label1)
# 绘制第一象限

bg2 = BoxAnnotation(bottom=y_mean, left=x_mean,fill_alpha=0.1, fill_color='firebrick')
label2 = Label(x=0.7, y=0.55,text="大量大打折",text_font_size="10pt" )
p.add_layout(bg2)
p.add_layout(label2)
# 绘制第二象限

bg3 = BoxAnnotation(top=y_mean, right=x_mean,fill_alpha=0.1, fill_color='firebrick')
label3 = Label(x=0.1, y=0.80,text="少量少打折",text_font_size="10pt" )
p.add_layout(bg3)
p.add_layout(label3)
# 绘制第三象限

bg4 = BoxAnnotation(top=y_mean, left=x_mean,fill_alpha=0.1, fill_color='olive')
label4 = Label(x=0.7, y=0.80,text="少量大打折",text_font_size="10pt" )
p.add_layout(bg4)
p.add_layout(label4)
# 绘制第四象限

show(p)
