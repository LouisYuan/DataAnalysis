# coding=utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings('ignore')

from bokeh.io import output_notebook

output_notebook()

from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource

from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False

import os

os.chdir('C:/Users/louis/DataAnalysis/source/项目10')

df01 = pd.read_csv('house_rent.csv', engine='python')
df02 = pd.read_csv('house_sell.csv', engine='python')

df01.dropna(inplace=True)
df02.dropna(inplace=True)

df01['rent_area'] = df01['price'] / df01['area']
data_rent = df01[['community', 'rent_area', 'lng', 'lat']].groupby(by='community').mean()
data_sell = df02[['property_name', 'average_price', 'lng', 'lat']].groupby(by='property_name').mean()
data_rent.reset_index(inplace=True)
data_sell.reset_index(inplace=True)

print('租房数据量为%i条' % len(data_rent))
print('售房数据量为%i条' % len(data_sell))

data = pd.merge(data_rent, data_sell, left_on='community', right_on='property_name')
data = data[['community', 'rent_area', 'average_price', 'lng_x', 'lat_x']]
data.rename(columns={'average_price': 'sell_area', 'lng_x': 'lng', 'lat_x': 'lat'}, inplace=True)  # 调整列名
print('合并后数据量为%i条' % len(data))

data['sell_rent'] = data['sell_area'] / data['rent_area']
print('上海房屋租售比中位数为%i个月' % data['sell_rent'].median())

data['sell_rent'].plot.hist(stacked=True, bins=100, color='green', alpha=0.5, grid=True, figsize=(10, 4))
plt.title('房屋售租比-直方图')
plt.savefig("房屋售租比.png")

# 绘制箱型图
color = dict(boxes='DarkGreen', whiskers='DarkOrange', medians='DarkBlue', caps='Gray')
data['sell_rent'].plot.box(vert=False, grid = True,color = color,figsize = (10,4))
plt.title('房屋售租比-箱型图')

plt.savefig('房屋售租比-箱线图.png')

data.to_csv('pro10data.csv')
