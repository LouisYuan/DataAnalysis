# coding=utf-8
# 项目04_视频网站数据清洗整理和结论研究
# 问答题 （100分） 作业要求：
# 1、数据清洗 - 去除空值
# 要求：创建函数
# 提示：fillna方法填充缺失数据，注意inplace参数
# 2、数据清洗 - 时间标签转化
# 要求：
# ① 将时间字段改为时间标签
# ② 创建函数
# 提示：
# 需要将中文日期转化为非中文日期，例如 2016年5月24日 → 2016.5.24
# 3、问题1 分析出不同导演电影的好评率，并筛选出TOP20
# 要求：
# ① 计算统计出不同导演的好评率，不要求创建函数
# ② 通过多系列柱状图，做图表可视化
# 提示：
# ① 好评率 = 好评数 / 评分人数
# ② 可自己设定图表风格
# 4、问题2 统计分析2001-2016年每年评影人数总量
# 要求：
# ① 计算统计出2001-2016年每年评影人数总量，不要求创建函数
# ② 通过面积图，做图表可视化，分析每年人数总量变化规律
# ③ 验证是否有异常值（极度异常）
# ④ 创建函数分析出数据外限最大最小值）
# ⑤ 筛选查看异常值 → 是否异常值就是每年的热门电影？
# 提示：
# ① 通过箱型图验证异常值情况
# ② 通过quantile(q=0.5)方法，得到四分位数
# ③ IQR=Q3-Q1
# ④ 外限：最大值区间Q3+3IQR,最小值区间Q1-3IQR （IQR=Q3-Q1）
# ⑤ 可自己设定图表风格

import pandas as pd
import matplotlib.pyplot as plt


def f1(df):
    cols = df.columns
    for col in cols:
        if df[col].dtype == 'object':
            df[col].fillna('缺失数据', inplace=True)
        else:
            df[col].fillna(0, inplace=True)
    return df


def f2(df, *cols):
    for col in cols:
        df[col] = df[col].str.replace("年",'.')
        df[col] = df[col].str.replace("月",'.')
        df[col] = df[col].str.replace("日",'.')
        df[col] = pd.to_datetime(df[col])
    return df


from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['FangSong'] # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False # 解决保存图像是负号'-'显示为方块的问题
data = pd.read_csv('C:/Users/louis/Desktop/Python数据分析学习/项目04视频网站数据清洗整理和结论研究/爱奇艺视频数据.csv', engine='python')
# print(data.head())
data1 = f1(data)
data2 = f2(data1,"数据获取日期")
print(data1.head(10))

goodCom = data2.groupby('导演')[['好评数',"评分人数"]].sum()
goodCom["好评率"] = goodCom["好评数"]/goodCom['评分人数']
result = goodCom.sort_values(["好评率"], ascending=False)[:20]

result["好评率"].plot(kind='bar',ylim = [0.98,1],figsize = (12,4),title = '不同导演电影的好评率')
# plt.show()

# 删除重复
q2data1 = data2[['导演','上映年份','整理后剧名']].drop_duplicates()

q2data1 = q2data1[q2data1['上映年份'] != 0]
q2data2 = data2.groupby('整理后剧名').sum()[['评分人数','好评数']]
# print(q2data2)
# 求出不同剧的评分人数、好评数总和

q2data3 = pd.merge(q2data1,q2data2,left_on='整理后剧名',right_index=True)
#print(q2data3)
# 合并数据，得到不同年份，不同剧的评分人数、好评数总和

q2data4 = q2data3.groupby('上映年份').sum()[['评分人数','好评数']]
print(q2data4.head())
# 按照电影上映年份统计，评分人数量

fig1 = plt.figure(num=1,figsize=(12,4))
plt.show()
q2data4['评分人数'].loc[2000:].plot.area(figsize = (10,4),
                                    grid = True,
                                    color = 'g',
                                    alpha = 0.8)
plt.xticks(range(2001,2016))
plt.title('影评人数求和')
plt.show()
fig,axes = plt.subplots(4,4,figsize=(10,16))
start = 2001
for i in range(4):
    for j in range(4):
        data = q2data3[q2data3['上映年份'] == start]
        data[['评分人数','好评数']].boxplot(whis=2,return_type='dict',ax = axes[i,j])
        start += 1

a = q2data3[q2data3['上映年份'] == 2001]
def limit(df, col):
    iqr = df[col].quantile(q=0.25)  - df[col].quantile(q=0.75)
    tmax = df[col].quantile(q=0.75) + 3 * iqr
    tmin = df[col].quantile(q=0.75) - 3 * iqr
    return tmax,tmin
# 创建函数，得到外限最大最小值

for i in range(2000,2016):
    datayear = q2data3[q2data3['上映年份'] == i]  # 筛选该年度的数据
    print('%i年有%i条数据' % (i,len(datayear)))  # 查看每年的数据量
    t = limit(datayear, '评分人数')  # 得到外限最大最小值
    #print(t)
    print(datayear[datayear['评分人数'] > t[0]])  # 查看评分人数大于外限最大值的异常值
    print('-------\n')
# plt.show()