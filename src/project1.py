# coding=utf-8

# 项目01_商铺数据加载及存储
import pandas as pd
import pickle
# 1、成功读取“商铺数据.csv”文件
# 2、解析数据，存成列表字典格式：[{'var1':value1,'var2':value2,'var3':values,...},...,{}]
# 3、数据清洗：
# ① comment，price两个字段清洗成数字
# ② 清除字段缺失的数据
# ③ commentlist拆分成三个字段，并且清洗成数字
# 4、结果存为.pkl文件

def clear_com(line):
    if "条" in line :
        # pass
        return (int(line.split(" ")[0]))
    else:
        return "缺失数据"

def clear_price(line):
    if "-"not in line:
        return line.split("￥")[-1]
    else:
        return "缺失数据"

def clean_lis(line):
    # print(line)
    if len(line)==3:
        quality = float(line[0][2:])
        envi = float(line[1][2:])
        service = float(line[2][2:])
        return [quality,envi,service]
    else:
        return "缺失数据"

num = 0
datalist = []
database = open('C:/Users/louis/Desktop/data.csv',encoding ='utf-8')
# print(database.readline())
for line in database:
    com =  clear_com(line.split(',')[2])
    # print(com)
    pri = clear_price(line.split(',')[4])
    # print(pri)
    comlist = clean_lis(line.split(",")[-1].split("                                "))
    # print(comlist)
    if com == '缺失数据':
        continue
    elif pri == '缺失数据':
        continue
    elif comlist == '缺失数据':
        continue
    else:
        classify = line.split(',')[0]
        name = line.split(',')[1]
        star = line.split(',')[3]
        price = pri
        add = line.split(',')[5]
        qua = comlist[0]
        env = comlist[1]
        ser = comlist[2]

        data_re = [['classify', classify],
                   ['name', name],
                   ['comment_count', com],
                   ['star', star],
                   ['price', price],
                   ['address', add],
                   ['quality', qua],
                   ['environment', env],
                   ['service', ser]]
        datalist.append(dict(data_re))  # 生成字典，并存入列表datalst
        num += 1

print(datalist)
print(num)
# 数据存储.pkl数据

pic = open('C:/Users/louis/Desktop/data.pkl','wb')
pickle.dump(datalist,pic)
pic.close()