# coding=utf-8
# 项目05_多场景下的算法构建
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import os

# % matplotlib inline
DataPath = 'C:/Users/louis/Desktop/source/'
PicPath = 'C:/Users/louis/Desktop/pic/'


def readData():
    folder = os.walk(DataPath)
    files = list(folder)[0][2]
    print(files)
    n = 1
    for i in files:
        if not i.split(".")[1] == "xlsx":
            continue
        file = DataPath + i
        data = pd.read_excel(file, index_col=0)
        print(data.head())
        print('第%i个数据数据量为：%i' % (n, len(data)))
        print('第%i个数据数据字段为：' % n, data.columns.tolist())
        print('第%i个数据缺失值数量为：%i' % (n, len(data[data.isnull().values == True])))
        n += 1


def getData():
    folder = os.walk(DataPath)
    files = list(folder)[0][2]
    data_files = []
    for i in files:
        if not i.split(".")[1] == "xlsx":
            continue
        file = DataPath + i
        data = pd.read_excel(file, index_col=0)
        columns = data.columns.tolist()
        data.to_period()
        data[columns[0]].fillna(data[columns[0]].mean(), inplace=True)
        data[columns[1]].fillna(data[columns[1]].mean(), inplace=True)
        data_files.append(data)
    return data_files


def fig1(*data_files):
    A_sale = []
    B_sale = []
    for data in data_files:
        columns = data.columns
        A_sale.append(data[columns[0]].sum())
        B_sale.append(data[columns[1]].sum())
    df = pd.DataFrame({'A_sale_sum': A_sale, 'B_sale_sum': B_sale},
                      index=pd.period_range('201801', '201803', freq='M'))
    print(df.head())
    plt.figure()
    df.plot(kind='bar', style='--o', color=['r', 'b'], alpha=0.8, rot=0, figsize=(8, 4))
    plt.title('1-3月A,B产品总销量柱状图')
    plt.ylim([0, 25000])
    plt.legend(loc='upper left')
    plt.grid()
    plt.savefig(PicPath + '1-3月A,B产品总销量柱状图.png', dpi=400)


def fig2(*data_files):
    key_dates = []
    for data in data_files:
        columns = data.columns
        data['A_sale_sum%'] = data[columns[0]].cumsum() / data[columns[0]].sum()  # 计算A产品累计销量占比
        key = data[data['A_sale_sum%'] > 0.8].index[0]
        key_dates.append(str(key))
    print('A产品月度超过80%的销量日期分别为\n', key_dates)
    return key_dates


def converTime():
    folder = os.walk(DataPath)  # 遍历文件夹
    files = list(folder)[0][2]
    data_files = []
    for i in files:
        if not i.split(".")[1] == "xlsx":
            continue
        file = DataPath + i
        data = pd.read_excel(file, index_col=0)
        data.to_period()  # 转换成时间序列
        data.dropna(inplace=True)  # 删除缺失值
        data_files.append(data)
    data = pd.concat([data_files[0],
                      data_files[1],
                      data_files[2]])  # 合并数据
    return data


def fig3(data):
    model = LinearRegression()
    model.fit(data['productA'][:, np.newaxis], data['productB'])
    # 构建回归模型
    xtest = np.linspace(0, 1000, 1000)
    ytest = model.predict(xtest[:, np.newaxis])
    plt.scatter(data['productA'], data['productB'], marker='.', color='k')
    plt.plot(xtest, ytest, color='r')
    plt.grid(True)
    plt.title('A-B产品销量回归拟合')
    plt.savefig(PicPath + 'A-B产品销量回归拟合.png', dpi=400)
    return (model.predict(1200))


# 题目1
readData()
# (data1, data2, data3) = getData()
# 题目2
fig1(getData())
fig2(getData())
#题目3
print('预测当A销量为1200时，B产品销量值为%.1f' % fig3(converTime()))