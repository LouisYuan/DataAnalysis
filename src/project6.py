# coding=utf-8
# 项目06_多场景下的图表可视化表达
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# % matplotlib inline
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题
import warnings

warnings.filterwarnings('ignore')
DataPath = 'C:/Users/louis/Desktop/source/'

file = DataPath + "奥运运动员数据.xlsx"
df = pd.read_excel(file, sheetname=1, header=0)
df_columns = df.columns.tolist()
data = df[['event', 'name', 'gender', 'height']]
data.dropna(inplace=True)
male = data[data["gender"] == "男"]
female = data[data["gender"] == "女"]

arrange_male = male["height"].mean()
arrange_female = female["height"].mean()

sns.set_style("dark")

sns.distplot(male['height'], hist=False, kde=True, rug=True,
             rug_kws={'color': 'y', 'lw': 2, 'alpha': 0.5, 'height': 0.1},
             kde_kws={"color": "y", "lw": 1.5, 'linestyle': ':'},
             label='maleHeight')
sns.distplot(female['height'], hist=False, kde=True, rug=True,
             rug_kws={'color': 'g', 'lw': 2, 'alpha': 0.5},
             kde_kws={"color": "g", "lw": 1.5, 'linestyle': ':'},
             label='femaleHeight')
plt.axvline(arrange_male, color='y', linestyle=":", alpha=0.8)
plt.axvline(arrange_female, color='g', linestyle=":", alpha=0.8)
plt.ylim([0, 0.03])
plt.grid(linestyle='--')
plt.title("Athlete's height")
plt.show()

df = pd.read_excel(file, sheetname=1, header=0)
df_columns = df.columns.tolist()
data = df[['event', 'name', 'birthday', 'height', 'arm', 'leg', 'weight', 'age']]
data.dropna(inplace=True)

data['BMI'] = data['weight'] / (data['height'] / 100) ** 2
data['leg/h'] = data['leg'] / data['height']
data['arm/h'] = data['arm'] / data['height']

data = data[data['leg/h'] < 0.7]
data = data[data['arm/h'] > 0.7]

data_re = data[['event', 'name', 'arm/h', 'leg/h', 'BMI', 'age']]
data_re['BMI_assess'] = np.abs(data['BMI'] - 22)
data_re['leg_assess'] = data['leg/h']
data_re['arm_assess'] = np.abs(data['arm/h'] - 1)
data_re['age_assess'] = data['age']

# 归一化
data_re['BMI_nor'] = (data_re['BMI_assess'].max() - data_re['BMI_assess']) / (
        data_re['BMI_assess'].max() - data_re['BMI_assess'].min())
data_re['leg_nor'] = (data_re['leg_assess'] - data_re['leg_assess'].min()) / (
        data_re['leg_assess'].max() - data_re['leg_assess'].min())
data_re['arm_nor'] = (data_re['arm_assess'].max() - data_re['arm_assess']) / (
        data_re['arm_assess'].max() - data_re['arm_assess'].min())
data_re['age_nor'] = (data_re['age_assess'].max() - data_re['age_assess']) / (
        data_re['age_assess'].max() - data_re['age_assess'].min())

data_re['final'] = (data_re['BMI_nor'] + data_re['leg_nor'] + data_re['arm_nor'] + data_re['age_nor']) / 4
plt.figure(figsize=(10, 6))
data_re.sort_values(by='final', inplace=True, ascending=False)
data_re.reset_index(inplace=True)

data_re[['age_nor', 'BMI_nor', 'leg_nor', 'arm_nor']].plot.area(colormap='cool', alpha=0.5, figsize=(10, 6))
plt.ylim([0, 4])
plt.grid(linestyle='--')
plt.show()

datatop8 = data_re[:8]

fig = plt.figure(figsize=(15, 6))
plt.subplots_adjust(wspace=0.35, hspace=0.5)

n = 0
for i in datatop8['name'].tolist():
    n += 1
    axi = plt.subplot(2, 4, n, projection='polar')
    datai = datatop8[['BMI_nor', 'leg_nor', 'arm_nor', 'age_nor']][datatop8['name'] == i].T
    scorei = datatop8['final'][datatop8['name'] == i]
    angles = np.linspace(0, 2 * np.pi, 4, endpoint=False)
    plt.polar(angles, datai, 'o-', linewidth=1)
    axi.fill(angles, datai, alpha=0.5)
    axi.set_thetagrids(np.arange(0.0, 360.0, 90), ['BMI', '腿长/身高', '臂长/身高', '年龄'])
    axi.set_rgrids(np.arange(0.2, 1.5, 0.2), '--')
    plt.title('Top%i %s: %.3f\n' % (n, i, scorei))
plt.show()

df = pd.read_excel(file, sheetname=2, header=0)
df_columns = df.columns.tolist()
df.replace([np.nan, '无数据', '无贴吧'], 0, inplace=True)
# 归一化
df['n1'] = (df['cp微博数量'] - df['cp微博数量'].min()) / (df['cp微博数量'].max() - df['cp微博数量'].min())
df['n2'] = (df['cp微博话题阅读量'] - df['cp微博话题阅读量'].min()) / (df['cp微博话题阅读量'].max() - df['cp微博话题阅读量'].min())
df['n3'] = (df['B站cp视频播放量'] - df['B站cp视频播放量'].min()) / (df['B站cp视频播放量'].max() - df['B站cp视频播放量'].min())
df['finalscore'] = df['n1'] * 0.5 + df['n2'] * 0.3 + df['n3'] * 0.2

df.sort_values(by='finalscore', inplace=True, ascending=False)
df.reset_index(inplace=True)

result = df[['p1', 'p2', 'finalscore']]
writer = pd.ExcelWriter(path=DataPath + 'output.xlsx')
result.to_excel(writer, 'sheet1')
writer.save()
