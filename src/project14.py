# coding=utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import time
import warnings
from bokeh.plotting import figure, show, output_file
warnings.filterwarnings("ignore")
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource, HoverTool
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False
# 创建数据
os.chdir('C:/Users/louis/DataAnalysis/source/项目14')
data_norm = pd.DataFrame({"正态分布": np.random.normal(loc=60, scale=15, size=10000)})
data_exp = pd.DataFrame({"指数分布": np.random.exponential(scale=15, size=10000) + 45})


# fig, axes = plt.subplots(1, 2, figsize=(12, 4))


# data_exp.hist(bins=50)
# data_norm.hist(bins=50)
#
# plt.savefig("")


def create_sample(n, gender):
    sample_data = pd.DataFrame({
        'fortune': np.random.exponential(scale=15, size=n) + 45,
        'appearance': np.random.normal(loc=60, scale=15, size=n),
        'character': np.random.normal(loc=60, scale=15, size=n)
    }, index=[gender + str(i) for i in range(1, n + 1)])
    sample_data.index.name = "id"
    sample_data["score"] = sample_data.sum(axis=1) / 3
    return sample_data


sample_m = create_sample(10000, "m")
sample_f = create_sample(10000, "f")

fig, axes = plt.subplots(2, 1, figsize=(12, 8))
sample_m[["appearance", "fortune", "character"]].iloc[:30].plot(kind="bar",
                                                                colormap="Blues_r", stacked=True, grid=True, ax=axes[0])
sample_f[["appearance", "fortune", "character"]].iloc[:30].plot(kind="bar",
                                                                colormap="Reds_r", stacked=True, grid=True, ax=axes[1])
# plt.show()
plt.savefig('data.png')

sample_m_test = create_sample(99, "m")
sample_f_test = create_sample(99, "f")
sample_m_test["strategy"] = np.random.choice([1, 2, 3], 99)

match_success = pd.DataFrame(columns=['m', 'f', 'round_n', 'strategy_type'])

round1_f = sample_f_test.copy()
round1_m = sample_m_test.copy()

round1_m['choice'] = np.random.choice(round1_f.index, len(round1_m))
round1_match = pd.merge(round1_m, round1_f, left_on='choice', right_index=True).reset_index()
round1_match['score_dis'] = np.abs(round1_match['score_x'] - round1_match['score_y'])
round1_match['cha_dis'] = np.abs(round1_match['character_x'] - round1_match['character_y'])
round1_match['for_dis'] = np.abs(round1_match['fortune_x'] - round1_match['fortune_y'])
round1_match['app_dis'] = np.abs(round1_match['appearance_x'] - round1_match['appearance_y'])  #
# 1
round1_s1_m = round1_match[round1_match['strategy'] == 1]
round1_s1_success = round1_s1_m[round1_s1_m['score_dis'] <= 20].groupby('choice').max()
round1_s1_success = pd.merge(round1_s1_success, round1_m.reset_index(), left_on='score_x', right_on='score')[
    ['id_y', 'choice']]
round1_s1_success.columns = ['m', 'f']
round1_s1_success['strategy_type'] = 1
round1_s1_success['round_n'] = 1
round1_match.index = round1_match['choice']
round1_match = round1_match.drop(round1_s1_success['f'].tolist())

# 2
round1_s2_m = round1_match[round1_match['strategy'] == 2]
round1_s2_success = round1_s2_m[(round1_s2_m['fortune_x'] - round1_s2_m['fortune_y'] >= 10) &
                                (round1_s2_m['appearance_y'] - round1_s2_m['appearance_x'] >= 10)]
round1_s2_success = round1_s2_success.groupby('choice').max()
round1_s2_success = pd.merge(round1_s2_success, round1_m.reset_index(), left_on='score_x', right_on='score')[
    ['id_y', 'choice']]
round1_s2_success.columns = ['m', 'f']
round1_s2_success['strategy_type'] = 2
round1_s2_success['round_n'] = 1
round1_match.index = round1_match['choice']
round1_match = round1_match.drop(round1_s2_success['f'].tolist())  # 删除策略2成功匹配的女性数据

# 3
round1_s3_m = round1_match[round1_match['strategy'] == 3]
round1_s3_success = round1_s3_m[(round1_s3_m['cha_dis'] < 10) &
                                (round1_s3_m['for_dis'] < 5) &
                                (round1_s3_m['app_dis'] < 5)]
round1_s3_success = round1_s3_success.groupby('choice').max()
round1_s3_success = pd.merge(round1_s3_success, round1_m.reset_index(), left_on='score_x', right_on='score')[
    ['id_y', 'choice']]
round1_s3_success.columns = ['m', 'f']
round1_s3_success['strategy_type'] = 3
round1_s3_success['round_n'] = 1

# 成功匹配数据
match_success = pd.concat([match_success, round1_s1_success, round1_s2_success, round1_s3_success])

# 下一轮实验数据
round2_m = round1_m.drop(match_success['m'].tolist())
round2_f = round1_f.drop(match_success['f'].tolist())

match_success.head()


# 构建模型


def different_strategy(data_m, data_f, roundnum):
    # 该轮的匹配选择
    data_m['choice'] = np.random.choice(data_f.index, len(data_m))
    round_match = pd.merge(data_m, data_f, left_on='choice', right_index=True).reset_index()
    round_match['score_dis'] = np.abs(round_match['score_x'] - round_match['score_y'])
    round_match['cha_dis'] = np.abs(round_match['character_x'] - round_match['character_y'])
    round_match['for_dis'] = np.abs(round_match['fortune_x'] - round_match['fortune_y'])
    round_match['app_dis'] = np.abs(round_match['appearance_x'] - round_match['appearance_y'])

    # 1
    s1_m = round_match[round_match['strategy'] == 1]
    s1_success = s1_m[s1_m['score_dis'] <= 20].groupby('choice').max()
    s1_success = pd.merge(s1_success, data_m.reset_index(), left_on='score_x', right_on='score')[['id_y', 'choice']]
    s1_success.columns = ['m', 'f']
    s1_success['strategy_type'] = 1
    s1_success['round_n'] = roundnum
    round_match.index = round_match['choice']
    round_match = round_match.drop(s1_success['f'].tolist())

    # 2
    s2_m = round_match[round_match['strategy'] == 2]
    s2_success = s2_m[
        (s2_m['fortune_x'] - s2_m['fortune_y'] >= 10) & (s2_m['appearance_y'] - s2_m['appearance_x'] >= 10)]
    s2_success = s2_success.groupby('choice').max()
    s2_success = pd.merge(s2_success, data_m.reset_index(), left_on='score_x', right_on='score')[['id_y', 'choice']]
    s2_success.columns = ['m', 'f']
    s2_success['strategy_type'] = 2
    s2_success['round_n'] = roundnum
    round_match.index = round_match['choice']
    round_match = round_match.drop(s2_success['f'].tolist())

    # 3
    s3_m = round_match[round_match['strategy'] == 3]
    s3_success = s3_m[(s3_m['cha_dis'] < 10) & (s3_m['for_dis'] < 5) & (s3_m['app_dis'] < 5)]
    s3_success = s3_success.groupby('choice').max()
    s3_success = pd.merge(s3_success, data_m.reset_index(), left_on='score_x', right_on='score')[['id_y', 'choice']]
    s3_success.columns = ['m', 'f']
    s3_success['strategy_type'] = 3
    s3_success['round_n'] = roundnum

    # 该轮成功匹配数据
    data_success = pd.concat([s1_success, s2_success, s3_success])

    return data_success


# 运行模型

sample_m1 = create_sample(10000, 'm')
sample_f1 = create_sample(10000, 'f')
sample_m1['strategy'] = np.random.choice([1, 2, 3], 10000)

test_m1 = sample_m1.copy()
test_f1 = sample_f1.copy()

n = 1
# 设定实验次数变量

success_roundn = different_strategy(test_m1, test_f1, n)
match_success1 = success_roundn
test_m1 = test_m1.drop(success_roundn['m'].tolist())
test_f1 = test_f1.drop(success_roundn['f'].tolist())
print('第%i轮，成功匹配%i对，共成功%i对，剩下%i位男性和%i位女性' %
      (n, len(success_roundn), len(match_success1), len(test_m1), len(test_f1)))
# 第一轮实验测试

while len(success_roundn) != 0:
    n += 1
    success_roundn = different_strategy(test_m1, test_f1, n)
    match_success1 = pd.concat([match_success1, success_roundn])
    test_m1 = test_m1.drop(success_roundn['m'].tolist())
    test_f1 = test_f1.drop(success_roundn['f'].tolist())
    # 输出下一轮实验数据
    print('第%i轮，成功匹配%i对，共成功%i对，剩下%i位男性和%i位女性' %
          (n, len(success_roundn), len(match_success1), len(test_m1), len(test_f1)))

print('进行了%i轮，配对成功%i对\n' % (n, len(match_success1)))

# 通过数据分析，回答几个问题：
#   ** 百分之多少的样本数据成功匹配到了对象？ 70.03%的样本数据成功匹配到了对象
#   ** 采取不同择偶策略的匹配成功率分别是多少？
# 择偶策略1的匹配成功率为100.00%
# 择偶策略2的匹配成功率为37.22%
# 择偶策略3的匹配成功率为73.30%
#   ** 采取不同择偶策略的男性各项平均分是多少？

# ① 百分之多少的样本数据成功匹配到了对象？
print('%.2f%%的样本数据成功匹配到了对象\n---------' % (len(match_success1) / len(sample_m1) * 100))

# ② 采取不同择偶策略的匹配成功率分别是多少？
print('择偶策略1的匹配成功率为%.2f%%' % (len(match_success1[match_success1['strategy_type'] == 1]) / len(
    sample_m1[sample_m1['strategy'] == 1]) * 100))
print('择偶策略2的匹配成功率为%.2f%%' % (len(match_success1[match_success1['strategy_type'] == 2]) / len(
    sample_m1[sample_m1['strategy'] == 2]) * 100))
print('择偶策略3的匹配成功率为%.2f%%' % (len(match_success1[match_success1['strategy_type'] == 3]) / len(
    sample_m1[sample_m1['strategy'] == 3]) * 100))
print('\n---------')

# ③ 采取不同择偶策略的男性各项平均分是多少？
match_m1 = pd.merge(match_success1, sample_m1, left_on='m', right_index=True)
result_df = pd.DataFrame([{'财富均值': match_m1[match_m1['strategy_type'] == 1]['fortune'].mean(),
                           '内涵均值': match_m1[match_m1['strategy_type'] == 1]['character'].mean(),
                           '外貌均值': match_m1[match_m1['strategy_type'] == 1]['appearance'].mean()},
                          {'财富均值': match_m1[match_m1['strategy_type'] == 2]['fortune'].mean(),
                           '内涵均值': match_m1[match_m1['strategy_type'] == 2]['character'].mean(),
                           '外貌均值': match_m1[match_m1['strategy_type'] == 2]['appearance'].mean()},
                          {'财富均值': match_m1[match_m1['strategy_type'] == 3]['fortune'].mean(),
                           '内涵均值': match_m1[match_m1['strategy_type'] == 3]['character'].mean(),
                           '外貌均值': match_m1[match_m1['strategy_type'] == 3]['appearance'].mean()}],
                         index=['择偶策略1', '择偶策略2', '择偶策略3'])

print('择偶策略1的男性 → 财富均值为%.2f，内涵均值为%.2f，外貌均值为%.2f' %
      (result_df.loc['择偶策略1']['财富均值'], result_df.loc['择偶策略1']['内涵均值'], result_df.loc['择偶策略1']['外貌均值']))
print('择偶策略2的男性 → 财富均值为%.2f，内涵均值为%.2f，外貌均值为%.2f' %
      (result_df.loc['择偶策略2']['财富均值'], result_df.loc['择偶策略2']['内涵均值'], result_df.loc['择偶策略2']['外貌均值']))
print('择偶策略3的男性 → 财富均值为%.2f，内涵均值为%.2f，外貌均值为%.2f' %
      (result_df.loc['择偶策略3']['财富均值'], result_df.loc['择偶策略3']['内涵均值'], result_df.loc['择偶策略3']['外貌均值']))

match_m1.boxplot(column=['fortune', 'character', 'appearance'], by='strategy_type', figsize=(10, 6), layout=(1, 3))
plt.ylim(0, 150)
plt.savefig("男性各项平均分.png")


sample_m2 = create_sample(99, 'm')
sample_f2 = create_sample(99, 'f')
sample_m2['strategy'] = np.random.choice([1, 2, 3], 99)

test_m2 = sample_m2.copy()
test_f2 = sample_f2.copy()

n = 1


success_roundn = different_strategy(test_m2, test_f2, n)
match_success2 = success_roundn
test_m2 = test_m2.drop(success_roundn['m'].tolist())
test_f2 = test_f2.drop(success_roundn['f'].tolist())
print('成功进行第%i轮实验，本轮实验成功匹配%i对，总共成功匹配%i对，还剩下%i位男性和%i位女性' %
      (n, len(success_roundn), len(match_success2), len(test_m2), len(test_f2)))
# 第一轮实验测试

while len(success_roundn) != 0:
    n += 1
    success_roundn = different_strategy(test_m2, test_f2, n)
    # 得到该轮成功匹配数据
    match_success2 = pd.concat([match_success2, success_roundn])
    # 将成功匹配数据汇总
    test_m2 = test_m2.drop(success_roundn['m'].tolist())
    test_f2 = test_f2.drop(success_roundn['f'].tolist())
    # 输出下一轮实验数据
    print('成功进行第%i轮实验，本轮实验成功匹配%i对，总共成功匹配%i对，还剩下%i位男性和%i位女性' %
          (n, len(success_roundn), len(match_success2), len(test_m2), len(test_f2)))
# 运行模型

print('------------')
print('本次实验总共进行了%i轮，配对成功%i对\n------------' % (n, len(match_success2)))

# 生成绘制数据表格

from bokeh.palettes import brewer

output_file('project14-1.html')
graphdata1 = match_success2.copy()
graphdata1 = pd.merge(graphdata1, sample_m2, left_on='m', right_index=True)
graphdata1 = pd.merge(graphdata1, sample_f2, left_on='f', right_index=True)

graphdata1['x'] = '0,' + graphdata1['f'].str[1:] + ',' + graphdata1['f'].str[1:]
graphdata1['x'] = graphdata1['x'].str.split(',')
graphdata1['y'] = graphdata1['m'].str[1:] + ',' + graphdata1['m'].str[1:] + ',0'
graphdata1['y'] = graphdata1['y'].str.split(',')

round_num = graphdata1['round_n'].max()
color = brewer['Blues'][round_num + 1]
graphdata1['color'] = ''
for rn in graphdata1['round_n'].value_counts().index:
    graphdata1['color'][graphdata1['round_n'] == rn] = color[rn - 1]

graphdata1 = graphdata1[['m', 'f', 'strategy_type', 'round_n', 'score_x', 'score_y', 'x', 'y', 'color']]

# bokeh绘图

p = figure(plot_width=500, plot_height=500, title="配对实验过程模拟示意", tools='reset,wheel_zoom,pan')  # 构建绘图空间

for datai in graphdata1.values:
    p.line(datai[-3], datai[-2], line_width=1, line_alpha=0.8, line_color=datai[-1], line_dash=[10, 4],
           legend='round %i' % datai[3])
    p.circle(datai[-3], datai[-2], size=3, color=datai[-1], legend='round %i' % datai[3])

p.ygrid.grid_line_dash = [6, 4]
p.xgrid.grid_line_dash = [6, 4]
p.legend.location = "top_right"
p.legend.click_policy = "hide"

show(p)


graphdata2 = match_success1.copy()
graphdata2 = pd.merge(graphdata2, sample_m1, left_on='m', right_index=True)
graphdata2 = pd.merge(graphdata2, sample_f1, left_on='f', right_index=True)

graphdata2 = graphdata2[
    ['m', 'f', 'appearance_x', 'character_x', 'fortune_x', 'appearance_y', 'character_y', 'fortune_y']]

graphdata2['for_m'] = pd.cut(graphdata2['fortune_x'], [0, 50, 70, 500], labels=['财低', '财中', '财高'])
graphdata2['cha_m'] = pd.cut(graphdata2['character_x'], [0, 50, 70, 500], labels=['品低', '品中', '品高'])
graphdata2['app_m'] = pd.cut(graphdata2['appearance_x'], [0, 50, 70, 500], labels=['颜低', '颜中', '颜高'])
graphdata2['for_f'] = pd.cut(graphdata2['fortune_y'], [0, 50, 70, 500], labels=['财低', '财中', '财高'])
graphdata2['cha_f'] = pd.cut(graphdata2['character_y'], [0, 50, 70, 500], labels=['品低', '品中', '品高'])
graphdata2['app_f'] = pd.cut(graphdata2['appearance_y'], [0, 50, 70, 500], labels=['颜低', '颜中', '颜高'])

graphdata2['type_m'] = graphdata2['for_m'].astype(np.str) + graphdata2['cha_m'].astype(np.str) + graphdata2[
    'app_m'].astype(np.str)
graphdata2['type_f'] = graphdata2['for_f'].astype(np.str) + graphdata2['cha_f'].astype(np.str) + graphdata2[
    'app_f'].astype(np.str)

graphdata2 = graphdata2[['m', 'f', 'type_m', 'type_f']]

success_n = len(graphdata2)
success_chance = graphdata2.groupby(['type_m', 'type_f']).count().reset_index()
success_chance['chance'] = success_chance['m'] / success_n
success_chance['alpha'] = (success_chance['chance'] - success_chance['chance'].min()) / (
        success_chance['chance'].max() - success_chance['chance'].min()) * 8  # 设置alpha参数
success_chance.head()

# bokeh绘图
output_file('project14-2.html')
mlst = success_chance['type_m'].value_counts().index.tolist()
flst = success_chance['type_f'].value_counts().index.tolist()
source = ColumnDataSource(success_chance)
hover = HoverTool(tooltips=[("男性类别", "@type_m"),
                            ("女性类别", "@type_f"),
                            ("匹配成功率", "@chance")])

p = figure(plot_width=800, plot_height=800, x_range=mlst, y_range=flst,
           title="不同类型男女配对成功率", x_axis_label='男', y_axis_label='女',
           tools=[hover, 'reset,wheel_zoom,pan,lasso_select'])

p.square_cross(x='type_m', y='type_f', source=source, size=18, color='red', alpha='alpha')

p.ygrid.grid_line_dash = [6, 4]
p.xgrid.grid_line_dash = [6, 4]
p.xaxis.major_label_orientation = "vertical"

show(p)
