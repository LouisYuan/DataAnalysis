# coding=utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
from sklearn import neighbors
import time
# 导入时间模块

import warnings

warnings.filterwarnings('ignore')

os.chdir('C:/Users/louis/DataAnalysis/source/项目15')
train_data = pd.read_csv('train.csv')
test_data = pd.read_csv('test.csv')
sns.set_style("ticks")
plt.axis('equal')
train_data['Survived'].value_counts().plot.pie(autopct='%1.2f%%')

# 年龄
sns.set()
sns.set_style("ticks")

train_data_age = train_data[train_data['Age'].notnull()]
plt.figure(figsize=(12, 5))
plt.subplot(121)
train_data_age['Age'].hist(bins=80)
plt.xlabel('Age')
plt.ylabel('Num')

plt.subplot(122)
train_data.boxplot(column='Age', showfliers=False)

# 性别
train_data[['Sex', 'Survived']].groupby(['Sex']).mean().plot.bar()
survive_sex = train_data.groupby(['Sex', 'Survived'])['Survived'].count()
# 女性存活率为74.20%，男性存活率为18.89%
print('女性存活率为%.2f%%，男性存活率为%.2f%%' %
      (survive_sex.loc['female', 1] / survive_sex.loc['female'].sum() * 100,
       survive_sex.loc['male', 1] / survive_sex.loc['male'].sum() * 100))

fig, ax = plt.subplots(1, 2, figsize=(18, 8))

sns.violinplot("Pclass", "Age", hue="Survived", data=train_data_age, split=True, ax=ax[0])
ax[0].set_title('Pclass and Age vs Survived')
ax[0].set_yticks(range(0, 110, 10))

sns.violinplot("Sex", "Age", hue="Survived", data=train_data_age, split=True, ax=ax[1])
ax[1].set_title('Sex and Age vs Survived')
ax[1].set_yticks(range(0, 110, 10))

# 年龄段
plt.figure(figsize=(18, 4))
train_data_age['Age_int'] = train_data_age['Age'].astype(int)
average_age = train_data_age[["Age_int", "Survived"]].groupby(['Age_int'], as_index=False).mean()
sns.barplot(x='Age_int', y='Survived', data=average_age, palette='BuPu')
plt.grid(linestyle='--', alpha=0.5)

# 亲人有无
sibsp_df = train_data[train_data['SibSp'] != 0]
no_sibsp_df = train_data[train_data['SibSp'] == 0]

parch_df = train_data[train_data['Parch'] != 0]
no_parch_df = train_data[train_data['Parch'] == 0]

plt.figure(figsize=(12, 3))
plt.subplot(141)
plt.axis('equal')
sibsp_df['Survived'].value_counts().plot.pie(labels=['No Survived', 'Survived'], autopct='%1.1f%%', colormap='Blues')
plt.xlabel('sibsp')

plt.subplot(142)
plt.axis('equal')
no_sibsp_df['Survived'].value_counts().plot.pie(labels=['No Survived', 'Survived'], autopct='%1.1f%%', colormap='Blues')
plt.xlabel('no_sibsp')

plt.subplot(143)
plt.axis('equal')
parch_df['Survived'].value_counts().plot.pie(labels=['No Survived', 'Survived'], autopct='%1.1f%%', colormap='Reds')
plt.xlabel('parch')

plt.subplot(144)
plt.axis('equal')
no_parch_df['Survived'].value_counts().plot.pie(labels=['No Survived', 'Survived'], autopct='%1.1f%%', colormap='Reds')
plt.xlabel('no_parch')

fig, ax = plt.subplots(1, 2, figsize=(15, 4))
train_data[['Parch', 'Survived']].groupby(['Parch']).mean().plot.bar(ax=ax[0])
ax[0].set_title('Parch and Survived')
train_data[['SibSp', 'Survived']].groupby(['SibSp']).mean().plot.bar(ax=ax[1])
ax[1].set_title('SibSp and Survived')

train_data['Family_Size'] = train_data['Parch'] + train_data['SibSp'] + 1
train_data[['Family_Size', 'Survived']].groupby(['Family_Size']).mean().plot.bar(figsize=(15, 4))

# 票价关系
fig, ax = plt.subplots(1, 2, figsize=(15, 4))
train_data['Fare'].hist(bins=70, ax=ax[0])
train_data.boxplot(column='Fare', by='Pclass', showfliers=False, ax=ax[1])
plt.show()

fare_not_survived = train_data['Fare'][train_data['Survived'] == 0]
fare_survived = train_data['Fare'][train_data['Survived'] == 1]

average_fare = pd.DataFrame([fare_not_survived.mean(), fare_survived.mean()])
std_fare = pd.DataFrame([fare_not_survived.std(), fare_survived.std()])
average_fare.plot(yerr=std_fare, kind='bar', legend=False, figsize=(15, 4), grid=True)
plt.show()

# 预测结果
knn_train = train_data[['Survived', 'Pclass', 'Sex', 'Age', 'Fare', 'Family_Size']].dropna()
knn_train['Sex'][knn_train['Sex'] == 'male'] = 1
knn_train['Sex'][knn_train['Sex'] == 'female'] = 0

test_data['Family_Size'] = test_data['Parch'] + test_data['SibSp'] + 1
knn_test = test_data[['Pclass', 'Sex', 'Age', 'Fare', 'Family_Size']].dropna()
knn_test['Sex'][knn_test['Sex'] == 'male'] = 1
knn_test['Sex'][knn_test['Sex'] == 'female'] = 0
print('清洗后训练集样本数据量为%i个' % len(knn_train))
print('清洗后测试集样本数据量为%i个' % len(knn_test))



knn = neighbors.KNeighborsClassifier()
knn.fit(knn_train[['Pclass','Sex','Age','Fare','Family_Size']], knn_train['Survived'])


knn_test['predict'] = knn.predict(knn_test)
pre_survived = knn_test[knn_test['predict'] == 1].reset_index()
del pre_survived['index']

